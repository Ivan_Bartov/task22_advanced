function random(number) {
    return Math.floor(Math.random() * (number+1));
}

function createTable() {
    if (document.contains(document.getElementById("my_table"))) 
        document.querySelector('#my_table').remove();

    var body = document.getElementsByTagName("body")[0];
    var m = document.getElementById("row").value;
    var n = document.getElementById("column").value;

    if(isNaN(parseInt(m)) || m=="") alert("Enter the number of rows");
    if(isNaN(parseInt(n)) || n=="") alert("Enter the number of columns");

    var table = document.createElement("table");
    table.id = "my_table";
    var table_body = document.createElement("tbody");
    for(var r = 0; r < m; r++) {
        var curr_row = document.createElement("tr");
        for(var c = 0; c < n; c++) {
            var curr_col = document.createElement("td");
            var text = document.createTextNode((r + 1).toString() + (c + 1).toString());
            curr_col.appendChild(text);
            curr_row.appendChild(curr_col);
        }
        table_body.appendChild(curr_row);
    }
    table.appendChild(table_body);
    body.appendChild(table);
    table.setAttribute("border", "1px");
}

document.getElementById("button").onclick = () => {
    createTable();

    document.getElementById("my_table").addEventListener("click", function(e) {
        var target = e.target;
        if (e.offsetX < 1 || e.offsetY < 1) {
            alert ("You have just clicked on border");
            return;
        }
        else {
            if (target) {
                e.target.style.backgroundColor = 'rgb(' + random(255) + ',' + random(255) + ',' + random(255) + ')';
            }
        }  
    });
}





